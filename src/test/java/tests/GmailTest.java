package tests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class GmailTest {
    private static final Logger log = LogManager.getLogger(GmailTest.class);
    WebDriver webDriver;
    String userEmail = "automationtest388@gmail.com";
    String userPassword = "4esZXdr5";
    String recipientEmail = "tetiana.marets@gmail.com";

    @BeforeTest
    public void setUp(){
        log.info("Create ChromeDriver");
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver");
        webDriver = new ChromeDriver();
        log.info("ChromeDriver has started successfully");
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        log.info("ImplicitlyWait timeout set to 10 seconds");
    }

    @Test
    public void gmailTest (){
        webDriver.get("https://mail.google.com/?");
        log.info("Page title is: " + webDriver.getTitle());

        WebElement loginEmail = webDriver.findElement(By.id("identifierId"));
        loginEmail.sendKeys(userEmail);
        log.info("Login email is: " + userEmail);
        WebElement nextButton = webDriver.findElement(By.xpath("//*[@id=\"identifierNext\"]/div/button/div[2]"));
        nextButton.click();

        WebElement password = webDriver.findElement(By.xpath("//input[@name='password']"));
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(password));
        password.sendKeys(userPassword);
        nextButton = webDriver.findElement(By.cssSelector("div.VfPpkd-RLmnJb"));
        nextButton.click();

        WebElement compose = webDriver.findElement(By.cssSelector("div.T-I.T-I-KE.L3"));
        compose.click();
        WebElement recipients = webDriver.findElement(By.id(":8y"));
        recipients.sendKeys(recipientEmail);

        WebElement subject = webDriver.findElement(By.name("subjectbox"));
        subject.sendKeys("TestEmail");

        WebElement message = webDriver.findElement(By.xpath("//*[@id=\":9l\"]"));
        message.sendKeys("It is a test message");

        WebElement sendButton = webDriver.findElement(By.xpath("//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']"));
        sendButton.click();

        boolean sentMessageTextIsPresent = (new WebDriverWait(webDriver,10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),'Message sent.')]")))
                .isEnabled();

        Assert.assertTrue(sentMessageTextIsPresent,"Message was not sent");
    }

    @AfterTest
    public void freeResources(){
        webDriver.quit();
        log.info("ChromeDriver has been closed successfully");
    }
}
